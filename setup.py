from setuptools import setup

setup(
    entry_points={
        'console_scripts': [
            'amrest_manage=amrest.manage:main'
        ]
    }
)
