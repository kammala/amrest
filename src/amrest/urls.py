from django.conf.urls import include, url
from rest_framework import routers

from amrest.categories.views import Category

router = routers.SimpleRouter()
router.register(r'category', Category, base_name='category')

urlpatterns = [
    url(r'^', include(router.urls, namespace='categories')),
]
