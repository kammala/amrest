from amrest.categories import models
from rest_framework import serializers


class SimpleCategory(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ('id', 'name')


class Category(serializers.ModelSerializer):
    siblings = SimpleCategory(many=True, read_only=True, source='get_siblings')
    parents = SimpleCategory(many=True, read_only=True, source='get_ancestors')
    children = SimpleCategory(many=True, source='get_children')

    class Meta:
        model = models.Category
        fields = ('id', 'name', 'siblings', 'parents', 'children')
