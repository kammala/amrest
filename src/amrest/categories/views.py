import logging

from django.db import transaction
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response

from amrest.categories import serializers, models

logger = logging.getLogger(__name__)


class Category(
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    model_class = models.Category
    serializer_class = serializers.Category

    def get_queryset(self):
        queryset = self.model_class.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        try:
            self.perform_create(request.data)
        except Exception as ex:
            logger.error('Invalid request', exc_info=True)
            return Response('Invalid request', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(None, status=status.HTTP_201_CREATED)

    def perform_create(self, data):
        # to perform data validation we may use smth like `jsonschema`
        # for first draft solution it is good enough to perform validation
        # on db side, converting any kind of exceptions into validation error
        with transaction.atomic():
            name = self._parse_name(data)
            root = self.model_class.add_root(name)
            self._create_children(root, data.get('children'))

    def _create_children(self, root, children):
        if not children:
            return
        for data in children:
            child = root.add_child(self._parse_name(data))
            self._create_children(child, data.get('children'))

    def _parse_name(self, data):
        name = data['name']
        if not isinstance(name, str):
            raise TypeError('Invalid name type: expected str, got {}'.format(name.__class__.__name__))
        return name
