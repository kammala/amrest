import pytest
from amrest.categories import models


class Categories:
    pass


@pytest.fixture
def categories(db):
    categories = Categories()
    categories.cat1 = models.Category.add_root(name='cat#1', order=1)
    categories.cat2 = models.Category.add_root(name='cat#2')
    categories.cat3 = models.Category.add_root(name='cat#3')
    categories.cat4 = models.Category.add_root(name='cat#4', order=100)

    categories.cat1_1 = categories.cat1.add_child('cat#1.subcat#1')
    categories.cat1_2 = categories.cat1.add_child('cat#1.subcat#2')

    categories.cat2_1 = categories.cat2.add_child('cat#2.subcat#1')

    categories.cat3_1 = categories.cat3.add_child('cat#3.subcat#1')
    categories.cat3_2 = categories.cat3.add_child('cat#3.subcat#2')
    categories.cat3_3 = categories.cat3.add_child('cat#3.subcat#3')

    categories.cat3_1_1 = categories.cat3_1.add_child('cat#3.subcat#1.subsub#1')
    categories.cat3_1_2 = categories.cat3_1.add_child('cat#3.subcat#1.subsub#2')
    categories.cat3_2_1 = categories.cat3_2.add_child('cat#3.subcat#2.subsub#1')
    categories.cat3_2_2 = categories.cat3_2.add_child('cat#3.subcat#2.subsub#2')
    categories.cat3_2_3 = categories.cat3_2.add_child('cat#3.subcat#2.subsub#3')

    return categories
