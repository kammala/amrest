from django.test import TestCase

from amrest.categories.models import Category


def get_names(qs):
    return [elem.name for elem in qs]


def test_get_siblings(categories):
    # first level
    assert get_names(categories.cat1.get_siblings()) == [
        categories.cat2.name, categories.cat3.name, categories.cat4.name
    ]
    assert get_names(categories.cat2.get_siblings()) == [
        categories.cat1.name, categories.cat3.name, categories.cat4.name
    ]
    assert get_names(categories.cat3.get_siblings()) == [
        categories.cat1.name, categories.cat2.name, categories.cat4.name
    ]
    
    # second level
    # only one sibling
    assert get_names(categories.cat1_1.get_siblings()) == [categories.cat1_2.name]
    assert get_names(categories.cat1_2.get_siblings()) == [categories.cat1_1.name]
    # no siblings
    assert not get_names(categories.cat2_1.get_siblings()) 
    # third level
    assert get_names(categories.cat3_2_1.get_siblings()) == [categories.cat3_2_2.name, categories.cat3_2_3.name]
    assert get_names(categories.cat3_2_2.get_siblings()) == [categories.cat3_2_1.name, categories.cat3_2_3.name]
    assert get_names(categories.cat3_2_3.get_siblings()) == [categories.cat3_2_1.name, categories.cat3_2_2.name]


def test_get_children(categories):
    # first level
    assert get_names(categories.cat1.get_children()) == [categories.cat1_1.name, categories.cat1_2.name]
    assert get_names(categories.cat2.get_children()) == [categories.cat2_1.name]
    assert get_names(categories.cat3.get_children()) == [categories.cat3_1.name, categories.cat3_2.name, categories.cat3_3.name]
    assert not categories.cat4.get_children()

    # second level
    assert not categories.cat1_1.get_children()
    assert not categories.cat1_2.get_children()

    assert not categories.cat2_1.get_children()

    assert get_names(categories.cat3_1.get_children()) == [categories.cat3_1_1.name, categories.cat3_1_2.name]
    assert get_names(categories.cat3_2.get_children()) == [categories.cat3_2_1.name, categories.cat3_2_2.name, categories.cat3_2_3.name]


def test_get_parents(categories):
    # first level
    assert not categories.cat1.get_ancestors()
    assert not categories.cat2.get_ancestors()
    assert not categories.cat3.get_ancestors()
    assert not categories.cat4.get_ancestors()

    # second level
    assert get_names(categories.cat1_1.get_ancestors()) == [categories.cat1.name]
    assert get_names(categories.cat1_2.get_ancestors()) == [categories.cat1.name]
    assert get_names(categories.cat2_1.get_ancestors()) == [categories.cat2.name]
    assert get_names(categories.cat3_1.get_ancestors()) == [categories.cat3.name]
    assert get_names(categories.cat3_2.get_ancestors()) == [categories.cat3.name]
    assert get_names(categories.cat3_3.get_ancestors()) == [categories.cat3.name]

    # third level
    assert get_names(categories.cat3_1_1.get_ancestors()) == [categories.cat3_1.name, categories.cat3.name]
    assert get_names(categories.cat3_2_1.get_ancestors()) == [categories.cat3_2.name, categories.cat3.name]
    assert get_names(categories.cat3_2_2.get_ancestors()) == [categories.cat3_2.name, categories.cat3.name]


def test_inserting_with_order(categories):
    last_name = 'last_item'
    categories.cat3_2.add_child(last_name, order=100)
    previous_brefore_last_name = 'previous_before_last_item'
    categories.cat3_2.add_child(previous_brefore_last_name, order=99)

    assert get_names(categories.cat3_2.get_children()) == [categories.cat3_2_1.name, categories.cat3_2_2.name, categories.cat3_2_3.name, previous_brefore_last_name, last_name,]
