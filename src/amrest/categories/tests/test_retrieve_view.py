import http

from django.urls.base import reverse


def category_to_json(category_object):
    return {
        'name': category_object.name,
        'id': category_object.id,
    }


def check_category_response(data, obj, children=None, ancestors=None, siblings=None):
    assert 'name' in data
    assert data['name'] == obj.name
    assert 'id' in data
    assert data['id'] == obj.id

    assert 'children' in data
    assert data['children'] == [
        category_to_json(c) for c in children or ()
    ]

    assert 'parents' in data
    assert data['parents'] == [
        category_to_json(c) for c in ancestors or ()
    ]

    assert 'siblings' in data
    assert data['siblings'] == [
        category_to_json(c) for c in siblings or ()
    ]


def test_ansent_category(categories, client):
    # simple check for test correctness
    resp = client.get(reverse('categories:category-detail', args=[categories.cat1.id]))
    assert resp.status_code == http.HTTPStatus.OK

    resp = client.get(reverse('categories:category-detail', args=[100500]))
    assert resp.status_code == http.HTTPStatus.NOT_FOUND


def test_root(categories, client):
    resp = client.get(reverse('categories:category-detail', args=[categories.cat1.id]))
    assert resp.status_code == http.HTTPStatus.OK

    data = resp.json()
    check_category_response(
        data=data,
        obj=categories.cat1,
        children=[categories.cat1_1, categories.cat1_2],
        siblings=[categories.cat2, categories.cat3, categories.cat4],
    )

    resp = client.get(reverse('categories:category-detail', args=[categories.cat4.id]))
    assert resp.status_code == http.HTTPStatus.OK

    data = resp.json()
    check_category_response(
        data=data,
        obj=categories.cat4,
        siblings=[categories.cat1, categories.cat2, categories.cat3]
    )


def test_leaf(categories, client):
    resp = client.get(reverse('categories:category-detail', args=[categories.cat3_2_1.id]))
    assert resp.status_code == http.HTTPStatus.OK

    data = resp.json()
    check_category_response(
        data=data,
        obj=categories.cat3_2_1,
        siblings=[categories.cat3_2_2, categories.cat3_2_3],
        ancestors=[categories.cat3_2, categories.cat3],
    )

    resp = client.get(reverse('categories:category-detail', args=[categories.cat2_1.id]))
    assert resp.status_code == http.HTTPStatus.OK

    data = resp.json()
    check_category_response(
        data=data,
        obj=categories.cat2_1,
        ancestors=[categories.cat2],
    )


def test_intermediate(categories, client):
    resp = client.get(reverse('categories:category-detail', args=[categories.cat3_2.id]))
    assert resp.status_code == http.HTTPStatus.OK

    data = resp.json()
    check_category_response(
        data=data,
        obj=categories.cat3_2,
        siblings=[categories.cat3_1, categories.cat3_3],
        ancestors=[categories.cat3],
        children=[categories.cat3_2_1, categories.cat3_2_2, categories.cat3_2_3],
    )
    resp = client.get(reverse('categories:category-detail', args=[categories.cat3_2.id]))
    assert resp.status_code == http.HTTPStatus.OK

