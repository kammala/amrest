import http

import json

import pytest
from django.urls import reverse


def _post_categories(
        client, data=None, raw_data=None
):
    if data is not None:
        raw_data = json.dumps(data)
    return client.post(
        reverse('categories:category-list'),
        content_type='application/json',
        data=raw_data
    )


@pytest.mark.parametrize(
    'data',
    [
        {'name': 'test'},
        {'name': 'test', 'children': []},
        {'name': 'test', 'children': None},
    ]
)
def test_good_case_single_level(data, client, db):
    resp = _post_categories(client, data=data)
    assert resp.status_code == http.HTTPStatus.CREATED


def test_good_case_multiple_level(categories, client, db):
    data = {
        'name': categories.cat1.name + '_new',
        'children': [
            {
                'name': categories.cat1.name,
                'children': [],
            },
            {
                'name': categories.cat3.name,
                'children': [
                    {'name': categories.cat3_1.name},
                    {'name': categories.cat3_2.name},
                ],
            },
        ],
    }
    resp = _post_categories(client, data=data)
    assert resp.status_code == http.HTTPStatus.CREATED


@pytest.mark.parametrize(
    'data',
    [
        {},
        {
            'name': [1, 2],
        },
        {
            'name': 'test',
            'children': {
                'name': 'test2'
            }
        },
        [],
        4,
        # inner level
        {
            'name': 'test',
            'children': [
                {'name': [1, 2]},
            ],
        },
        {
            'name': 'test',
            'children': [
                {'name': 'test'},
                [1, 2]
            ],
        },
    ]
)
def test_invalid_dataschema(data, client, db):
    resp = _post_categories(client, data=data)
    assert resp.status_code == http.HTTPStatus.BAD_REQUEST


@pytest.mark.parametrize(
    'data',
    [
        '{',
        '}',
        '[',
        '{"name: "test"}',
    ]
)
def test_invalid_json(data, client):
    resp = _post_categories(client, raw_data=data)
    assert resp.status_code == http.HTTPStatus.BAD_REQUEST


def test_name_conflicts_with_existing_root(categories, client):
    resp = _post_categories(client, data={'name': categories.cat1.name})
    assert resp.status_code == http.HTTPStatus.BAD_REQUEST


def test_name_conflicts_inside_input_leafs(client, db):
    resp = _post_categories(client, data={
        'name': 'new_category',
        'children': [
            {
                'name': 'subcategory',
            },
            {
                'name': 'subcategory_1',
            },
            {
                'name': 'subcategory',
            },
        ]
    })
    assert resp.status_code == http.HTTPStatus.BAD_REQUEST
