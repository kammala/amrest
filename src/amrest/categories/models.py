from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=256, null=False)
    parent = models.ForeignKey('Category', related_name='+', null=True)
    order = models.IntegerField(null=False)

    class Meta:
        unique_together = ('name', 'parent')
        ordering = ('parent_id', 'order')

    def __str__(self):  # pragma: no cover
        if self.parent is None:
            parent_info = 'root'
        else:
            parent_info = 'child of {}'.format(self.parent_id)
        return '{} [{}]'.format(self.name, parent_info)

    @classmethod
    def add_root(cls, name, order=None):
        if cls.objects.filter(parent=None, name=name).exists():
            # some dbs such as sqlite and postrgesql think that
            # null != null even in unique contraints
            # we should manually check uniqueness here
            raise ValueError('Root with name "{}" already exists'.format(name))
        if order is None:
            max_order = (
                cls.objects
                .filter(parent=None)
                .aggregate(max_order=models.Max('order'))['max_order']
            ) or 0
            order = max_order + 1
        obj = cls.objects.create(name=name, parent=None, order=order)
        ClosureTable.objects.create(
            ancestor=obj,
            descendant=obj,
            depth=0,
        )
        return obj

    def add_child(self, name, order=None):
        if order is None:
            max_order = (
                self.__class__.objects
                .filter(parent=self)
                .aggregate(max_order=models.Max('order'))
            )['max_order'] or 0
            order = max_order + 1
        obj = self.__class__.objects.create(name=name, parent=self, order=order)
        ClosureTable.objects.create(ancestor=obj, descendant=obj, depth=0)
        ClosureTable.objects.bulk_create([
            ClosureTable(
                ancestor=ct.ancestor,
                descendant=obj,
                depth=ct.depth + 1
            )
            for ct in ClosureTable.objects.filter(descendant=self)
        ])
        return obj

    def get_children(self):
        return [
            ct.descendant
            for ct in (
                ClosureTable.objects
                .filter(ancestor=self, depth=1)
                .order_by('descendant__order')
            )
        ]

    def get_siblings(self):
        if self.parent is None:
            return (
                self.__class__.objects
                .filter(parent__isnull=True)
                .exclude(pk=self.pk)
                .order_by('order')
            )
        return [
            ct.descendant
            for ct in (
                ClosureTable.objects
                .filter(ancestor=self.parent, depth=1)
                .exclude(descendant=self)
                .order_by('descendant__order')
            )
        ]

    def get_ancestors(self):
        return [
            ct.ancestor
            for ct in (
                ClosureTable.objects
                .filter(descendant=self, depth__gte=1)
                .order_by('depth')
            )
        ]


class ClosureTable(models.Model):
    ancestor = models.ForeignKey(Category, related_name='+', null=False)
    descendant = models.ForeignKey(Category, related_name='+', null=False)
    depth = models.IntegerField(null=False)

    class Meta:
        unique_together = ('ancestor', 'descendant')
        ordering = ('depth',)
